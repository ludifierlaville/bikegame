﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

	public enum ObstacleType {
		Roche,
		Flaque,
		Racoon,
		COUNT
	};
	public ObstacleType obstacleType;

	List<Transform> children = new List<Transform>();
	public AudioSource audio;

	void Start () {
		// Set all the children to true so that we can add them to list
		foreach (Transform child in transform) {
			child.gameObject.SetActive(true);
		}

		foreach (Transform child in transform) {
			children.Add(child);
		}

		// Set all the children to false
		foreach (Transform child in transform) {
			child.gameObject.SetActive(false);
		}

		DisplayObstacle (obstacleType.ToString());
	}

	public void DisplayObstacle (string obstacleName) {
		foreach (Transform child in transform) {
			if (child.gameObject.name == obstacleName) {
				child.gameObject.SetActive(true);
				audio = child.GetComponent<AudioSource>();
				children.Clear();
				return;
			}
		}
		Debug.Log("No children named " + obstacleName);
	}

	void OnTriggerEnter (Collider col) {
		if (col.GetComponent<Biker>() != null) {
			Hit (col.GetComponent<Biker>(), obstacleType.ToString());
		}
	}

	private void Hit (Biker biker, string obstacle) {
		if (obstacle == "Roche")
			biker.Pedal(-8);
		else if (obstacle == "Flaque")
			biker.Pedal(-4);
		else if (obstacle == "Racoon")
			biker.Pedal(-4);
		
		audio.Play();
	}
}
