﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedalInput : MonoBehaviour {

	private Biker biker;

    public CountdownBeforeStart cd;

    public KeyCode leftInput;
    public KeyCode rightInput;

    private bool pressedRight;

	void Start () 
    {
		cd = FindObjectOfType<CountdownBeforeStart>();

		if (this.tag != "Biker") 
        {
			Debug.LogError("Set the tag to \"Biker\"");
		}

		biker = GetComponent<Biker>();
	}
	
	void Update () 
    {
        if (!cd.gameHasStarted)
        {
            return;
        }

        //user needs to alternate pressing left and right (for our demo)
        if (Input.GetKeyDown(leftInput))
        {
            if (pressedRight)
            {
                pressedRight = false;
				biker.Pedal ();
            }
        }
        if (Input.GetKeyDown(rightInput))
        {
            if (!pressedRight)
            {
                pressedRight = true;
				biker.Pedal ();
            }
        }
	}
}
