﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetRandomObstacle : MonoBehaviour {

    public GameObject[] obstacles;
    int rand;

	void Start () 
    {
        rand = Random.Range(0, obstacles.Length);
        obstacles[rand].SetActive(true);
	}

    public void OnTriggerEnter(Collider other)
    {
		obstacles[rand].GetComponent<RockCollision>().OnTrigger(other.gameObject);
    }
}
