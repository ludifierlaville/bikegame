﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Places the rows based on the Top Speedline
/// </summary>
public class RowPlacer : MonoBehaviour {
	void Start () {
		PlaceRows ();
		PlaceBikers ();
	}

	private void PlaceRows () {
		float yScale = GameController.instance.rows[0].transform.localScale.z;

		// Set the y scale based on the first row
		for (int i = 1; i < GameController.instance.rows.Length; i++) {
			GameController.instance.rows[i].transform.localScale = new Vector3 (
				GameController.instance.rows[i].transform.localScale.x,
				GameController.instance.rows[i].transform.localScale.y,
				yScale
			);
		}
	
		// Set the y row spacing based on the first row
		for (int i = 0; i < GameController.instance.rows.Length; i++) {
			GameController.instance.rows[i].transform.position = new Vector3 (
				GameController.instance.rows[i].transform.position.x,
				GameController.instance.rows[i].transform.position.y,
				GameController.instance.rows[0].transform.position.z - (GameController.rowSpacing * i)
			);
		}

//		// Set the y row spacing based on the first row
//		for (int i = 0; i < rows.Length; i++) {
//			rows[i].transform.localPosition = new Vector3 (
//				rows[i].transform.localPosition.x,
//				rows[0].transform.localPosition.y - (GameController.rowSpacing * i),
//				rows[i].transform.localPosition.z
//			);
//		}
	}

	private void PlaceBikers () {
		// Place the bikers based on their start row
		for (int i = 0; i < GameController.instance.bikers.Length; i++) {
			int startRow = GameController.instance.bikers[i].GetComponent<RowSwitcher>().startRow;

			// Set the biker's y position to the row that it should start on (row was set right before)
			GameController.instance.bikers[i].transform.position = new Vector3 (
				GameController.instance.bikers[i].transform.position.x,
				GameController.instance.bikers[i].transform.position.y,
				GameController.instance.rows[startRow].transform.position.z
			);
		}
	}
}
