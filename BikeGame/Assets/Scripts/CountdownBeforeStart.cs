﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownBeforeStart : MonoBehaviour {

    public GameObject countdownText;
    public bool gameHasStarted = false;

    int timeLeft = 3;
	float ogScale = 1;

	[SerializeField]
	private float scaleSpeed = 1f;

	// Use this for initialization
	void Start () 
    {
//		ogScale = countdownText.GetComponent<TextMesh>().transform.localScale.x;
        StartCoroutine(Countdown());
	}

	void Update () {
		countdownText.GetComponent<TextMesh>().transform.localScale = new Vector3 (
			countdownText.transform.localScale.x + scaleSpeed * Time.deltaTime,
			countdownText.transform.localScale.y + scaleSpeed * Time.deltaTime,
			countdownText.transform.localScale.z + scaleSpeed * Time.deltaTime
		);
	}

    IEnumerator Countdown()
    {
        yield return new WaitForSeconds(1f);

        countdownText.SetActive(true);



        while (timeLeft > 0)
        {
			countdownText.GetComponent<TextMesh>().transform.localScale = Vector3.one;

            countdownText.GetComponent<TextMesh>().text = timeLeft.ToString();
            timeLeft -= 1;

            yield return new WaitForSeconds(1f);
        }

        countdownText.SetActive(false);
        gameHasStarted = true;
    }
}
