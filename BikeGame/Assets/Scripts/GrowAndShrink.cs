﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowAndShrink : MonoBehaviour {

	private float currentScale;

	void Start () {
		currentScale = transform.localScale.x;
	}

	void Update () {

		float scale = ((Mathf.PingPong(Time.time, 0.6f)*0.3f)+1f)*currentScale;

		transform.localScale = new Vector3(scale, scale, scale);
	}
}
