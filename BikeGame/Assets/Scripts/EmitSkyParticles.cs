﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitSkyParticles : MonoBehaviour {

    public ParticleSystem snowSystem;

	// Update is called once per frame
	public void OnNewDay (string skyCondition) 
    {
		//new meteo for that day, adjust particle systems
        if (snowSystem != null)
        {
            if (skyCondition == "Neige")
            {
                snowSystem.Play();
            }
            else
            {
                snowSystem.Stop();
            }
        }
	}
}
