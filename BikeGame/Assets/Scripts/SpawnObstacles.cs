﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacles : MonoBehaviour {

    [SerializeField]
    private GameObject obstaclePrefab = null;
    [SerializeField]
    private GameObject powerUpPrefab = null;
    [SerializeField]
    private GameObject background = null;
    [SerializeField]
    private GameObject backgroundPrefab = null;
    [SerializeField]
    private GameObject foreground = null;
    [SerializeField]
    private GameObject foregroundPrefab = null;
	[SerializeField]
	private GameObject treePrefab = null;

	private float finishLineX;
	private float startLineX;

    private float[] zPositions;
    private int[] zOccurences;

	[SerializeField]
    private float minObstacleDistance = 8f;
    private float maxObstacleDistance = 15f;

	[SerializeField]
	private int numPowerUps = 50;

	[SerializeField]
	private float backgroundLength = 8.1f;

	void Start () {

		// z positions for possible obstacles are one of the 4 lanes (hardcoded 4)
		zPositions = new float[4];
		for (int i = 0; i < zPositions.Length; i++) {
			zPositions[i] = GameController.instance.rows[i+1].transform.localPosition.z;
		}
        //same for occurences
        zOccurences = new int[4];
        for (int i = 0; i < zOccurences.Length; i++)
        {
            zOccurences[i] = 1;
        }

        //gives the range of x values to randomize
        //also leave some empty space after the starting line and before the finish line
		finishLineX = GameController.instance.finishLine.transform.localPosition.x;
        startLineX = GameController.instance.startLine.transform.localPosition.x;
        int totalDistance = (int)(finishLineX - startLineX);

        float xPos = maxObstacleDistance;
        //don't loop more than 150 times
        //(the current tweaking normally generates less than that)
        int secureMaxObstacleBank = 200;
        do
        {
            xPos += minObstacleDistance + Random.value * (maxObstacleDistance - minObstacleDistance);

            //create obstacle and make it child of course to benefit from its speed effect
			GameObject obstacle = (GameObject)Instantiate (obstaclePrefab);
			obstacle.transform.parent = this.transform;

            //pick one of the 4 lanes, but favors the one that's been free for the longest time
            //i.e. each times it's not picked increase the weight by 1
            float zPos = GetZPos();

            //place obstacle
			obstacle.transform.localPosition = new Vector3(
				xPos,
				0.306f,
				zPos);

            // We spawn more and more obstacles as we approach the end
            minObstacleDistance = minObstacleDistance * 0.99f;
            maxObstacleDistance = maxObstacleDistance * 0.999f;

            // Choose a random obstacle type
            obstacle.GetComponent<Obstacle>().obstacleType = (Obstacle.ObstacleType)Random.Range(0, (int)Obstacle.ObstacleType.COUNT);

            secureMaxObstacleBank--;
		}
        while (xPos < totalDistance - maxObstacleDistance && secureMaxObstacleBank > 0);

        //clear zOccurences
        for (int i = 0; i < zOccurences.Length; ++i)
        {
            zOccurences[i] = 1;
        }

		// Spawn powerups, evenly distributed, no constraints
		for (int i = 0; i < numPowerUps; i++) 
        {
			GameObject powerUp = (GameObject)Instantiate (powerUpPrefab);
			powerUp.transform.parent = this.transform;

			xPos = Random.Range(startLineX, finishLineX);
            //pick one of the 4 lanes, but favors the one that's been free for the longest time
            //i.e. each times it's not picked increase the weight by 1
			float zPos = GetZPos();

			powerUp.transform.localPosition = new Vector3(
				xPos,
				0.306f,
				zPos
			);

			// Choose a random power up type
			powerUp.GetComponent<PowerUp>().powerUpType = (PowerUp.PowerUpType)Random.Range(0, (int)PowerUp.PowerUpType.COUNT);

			// Don't spawn power ups over obstacles (that's just mean)
			Collider[] colliders = Physics.OverlapBox(powerUp.transform.position, Vector3.one);
			foreach (Collider col in colliders) {
				if (col.GetComponent<Obstacle>() != null) {
					Destroy(powerUp);
				}
			}
        }

        SpawnBackground(totalDistance);
        //SpawnForeground(totalDistance);
    }

    void SpawnBackground(int totalDistance)
    {
        for (float i = -backgroundLength * 2; i < totalDistance; i += backgroundLength)
        {
            GameObject background = (GameObject)Instantiate(backgroundPrefab);
            background.transform.parent = background.transform;

            float xPos = i;

            background.transform.localPosition = new Vector3(
                xPos,
                2.9f,
                0.75f
            );

            List<Transform> children = new List<Transform>();
            foreach (Transform child in background.transform)
            {
                children.Add(child);
            }

            int lotteryWinner = Random.Range(0, children.Count);

            for (int j = 0; j < children.Count; j++)
            {
                if (j == lotteryWinner)
                    children[j].gameObject.SetActive(true);
                else
                    children[j].gameObject.SetActive(false);
            }
        }
    }

    void SpawnForeground(int totalDistance)
    {
        for (float i = -backgroundLength * 2; i < totalDistance; i += backgroundLength)
        {
            GameObject foreground = (GameObject)Instantiate(foregroundPrefab);
            foreground.transform.parent = foreground.transform;

            float xPos = i;

            foreground.transform.localPosition = new Vector3(
                xPos,
                0.306f,
                -22.26f
            );

            List<Transform> children = new List<Transform>();
            foreach (Transform child in foreground.transform)
            {
                children.Add(child);
            }

            int lotteryWinner = Random.Range(0, children.Count);

            for (int j = 0; j < children.Count; j++)
            {
                if (j == lotteryWinner)
                    children[j].gameObject.SetActive(true);
                else
                    children[j].gameObject.SetActive(false);
            }
        }
    }

    float GetZPos()
    {
        float zPos = zPositions[0]; //default value

        int totalWeight = zOccurences[0] + zOccurences[1] + zOccurences[2] + zOccurences[3];
        int weightedRand = Random.Range(0, totalWeight);
        bool found = false;

        for (int i = 0; i < zPositions.Length; ++i)
        {
            //check lane by lane if it's a winning ticket
            if (!found && weightedRand < zOccurences[i])
            {
                zPos = zPositions[i];
                found = true;
                //reset weight for this one
                zOccurences[i] = 1;
            }
            else
            {
                //substract previous ticket numbers
                weightedRand -= zOccurences[i];
                //increase weight by one for next time
                zOccurences[i]++;
            }
        }
        return zPos;
    }
}
