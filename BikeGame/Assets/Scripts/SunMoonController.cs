﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMoonController : MonoBehaviour {

    public FollowLeader leader;

    float startRotation;
    float offset;

	// Use this for initialization
	void Start () 
    {
        startRotation = transform.rotation.z;
        offset = 0;

        if (leader == null)
        {
            leader = FindObjectOfType<FollowLeader>();
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        //change the day when leader pass me
        float leadPosition = leader.GetLeadPosition();
        Quaternion rot = transform.rotation;
        rot.z = (startRotation - offset) - leadPosition * 0.1f;
        transform.rotation = rot;
	}

    public void reset()
    {
        Quaternion rot = transform.rotation;
        offset += rot.z - startRotation;
    }
}
