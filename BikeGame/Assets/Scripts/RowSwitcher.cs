﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RowSwitcher : MonoBehaviour {

	public int startRow;

	int currentRow;
	int rowCount = 4;

	[SerializeField]
	private float moveTime = 0.5f;

	private bool moving = false;

	private AudioSource audio;
	[SerializeField]
	private AudioClip switchLaneSound;

	// Use this for initialization
	void Start ()
	{
		audio = GetComponent<AudioSource>();
		currentRow = startRow;
	}

	public void GoUp ()
	{
		if (currentRow > 1)
		{
			Move (1);
		}
	}

	public void GoDown ()
	{
		if (currentRow < rowCount)
		{
			Move (-1);
		}
	}

	public void Move (int direction) {
		Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, 0.5f); //0.5 --> to adjust to bike size

		bool colliding = false;

//		foreach (Collider col in hitColliders) {
//			if (col.transform != this.transform && col.tag == "Biker") {
//				colliding = true;
//			}
//		}

		if (!colliding &&  !moving) //no one with a collider here, proceed
		{
			// Play the sound
			audio.PlayOneShot(switchLaneSound);

			moving = true;
			StartCoroutine (SmoothVerticalMove(transform, direction));
		}
		else
		{
			//TODO: try to turn animation
		}
	}

	/// <summary>
	/// Smooths the vertical move.
	/// </summary>
	/// <returns>The vertical move.</returns>
	/// <param name="bike">The bike transform.</param>
	/// <param name="moveAmount">The amount to move up or down.</param>
	public IEnumerator SmoothVerticalMove (Transform bike, int direction) {


		float moveAmount = GameController.rowSpacing * direction;
		// The new y position added to current
		float newPosZ = bike.position.z + moveAmount;
		// The timer goes from 0 to 1
		float timer = 0;
		// Divide ahead of time
		float moveTimeDivided = 1/moveTime;

		Biker biker = GetComponent<Biker>();

		Quaternion turnTargetGuidon = biker.leftGuidonTarget.transform.localRotation;
		Quaternion originalRot = biker.transform.rotation;

		while (timer < 1f) {
			// Timer goes up at moveTime's speed
			timer += Time.deltaTime * moveTimeDivided;

			// Interpolate the new height based on timer
			bike.position = new Vector3 (
				bike.position.x,
				bike.position.y,
				Mathf.Lerp(bike.position.z, newPosZ, timer)
			);


			if (direction < 0) {
				turnTargetGuidon = biker.rightGuidonTarget.transform.localRotation;
			}

			if (timer < 0.5f) {
				biker.transform.Rotate(0, -direction*10, 0);
				biker.guidon.transform.localRotation = Quaternion.Slerp(biker.guidon.transform.localRotation, turnTargetGuidon, moveTimeDivided); 
			} else {
				biker.transform.localRotation = Quaternion.Slerp(biker.transform.localRotation, originalRot, moveTimeDivided);
				biker.guidon.transform.localRotation = Quaternion.Slerp(turnTargetGuidon, biker.defaultGuidonTarget.transform.localRotation, moveTimeDivided); 
			}

			yield return null;
		}

		bike.rotation = originalRot;

		// Just making sure the final height is correct
		bike.position = new Vector3 (
			bike.position.x,
			bike.position.y,
			newPosZ
		);

		// Set moving to false at the end
		moving = false;
		currentRow -= direction;
	}
}
