﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public static GameController instance;

    public static float firstRowOffset = 1.3f;
	public static float rowSpacing = 2.18f;
	public static float rowMoveTime = 0.5f;
	public static float defaultpedalForce = 10f;

	[SerializeField]
	private RenderTexture renderTexture;

	[SerializeField]
	private Camera gameCamera;
	[SerializeField]
	private Camera podiumCamera;

	public GameObject[] bikers;

	public GameObject[] rows;

	public GameObject startLine;
	public GameObject finishLine;

	private bool gameDone = false;

	[SerializeField]
	private SkinnedMeshRenderer[] podiumCharacterSMRs;

	void Awake () {
		instance = this;

		if (gameCamera == null) {
			Debug.LogError("Set the game camera in the inspector");
			return;
		}

		// TODO countdown before being able to pedal
	}

	void Update () {
		if (gameDone)
			return;

		if (Input.GetKeyDown(KeyCode.Z)) {
//			gameCamera.targetTexture = null;
//			podiumCamera.targetTexture = renderTexture;
		}

		if (Input.GetKeyDown(KeyCode.X)) {
//			gameCamera.targetTexture = renderTexture;
//			podiumCamera.targetTexture = null;
		}

		for (int i = 0; i < bikers.Length; i++) {
			if (bikers[i].transform.position.x > finishLine.transform.position.x) {
				StartCoroutine(GameOver (bikers[i].GetComponent<Biker>()));
				// Avoid more than one winner
				gameDone = true;
				break;
			}
		}
	}

	private IEnumerator GameOver (Biker winner) {
		
		// Remove all of these as soon as someone passes the finish line to stop all of the moving aroundness
		FollowLeader[] followLeaders = FindObjectsOfType<FollowLeader>();
		foreach (FollowLeader followLeader in followLeaders) {
			Destroy(followLeader);
		}

		yield return new WaitForSeconds(2f);

		// Change characters colour based on winner (this whole section is really badly coded, sorry)
		if (winner.playerColor == Biker.PlayerColor.Red) {
			podiumCharacterSMRs[0].material.color = Color.red;
			podiumCharacterSMRs[1].material.color = Color.green;
			podiumCharacterSMRs[2].material.color = Color.yellow;
		} else if (winner.playerColor == Biker.PlayerColor.Yellow) {
			podiumCharacterSMRs[0].material.color = Color.yellow;
			podiumCharacterSMRs[1].material.color = Color.blue;
			podiumCharacterSMRs[2].material.color = Color.red;
		} else if (winner.playerColor == Biker.PlayerColor.Green) {
			podiumCharacterSMRs[0].material.color = Color.green;
			podiumCharacterSMRs[1].material.color = Color.yellow;
			podiumCharacterSMRs[2].material.color = Color.blue;
		} else {
			podiumCharacterSMRs[0].material.color = Color.blue;
			podiumCharacterSMRs[1].material.color = Color.red;
			podiumCharacterSMRs[2].material.color = Color.yellow;
		}

			

		// Switch to podium view
		gameCamera.targetTexture = null;
		podiumCamera.targetTexture = renderTexture;

		yield return new WaitForSeconds(5f);

		// Restart the scene
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

}
