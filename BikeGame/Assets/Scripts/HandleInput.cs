﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleInput : MonoBehaviour {

	public CountdownBeforeStart cd;

    public KeyCode upInput;
    public KeyCode downInput;

    private RowSwitcher rowSwitcher;

	// Use this for initialization
	void Start () 
    {
		cd = FindObjectOfType<CountdownBeforeStart>();
        rowSwitcher = GetComponent<RowSwitcher>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!cd.gameHasStarted)
        {
            return;
        }

        //user jumps to another row when turning the handle
        if (Input.GetKeyDown(upInput))
        {
            rowSwitcher.GoUp();
        }
        if (Input.GetKeyDown(downInput))
        {
            rowSwitcher.GoDown();
        }
	}
}
