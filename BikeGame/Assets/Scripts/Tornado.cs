﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Tornado : MonoBehaviour {

	[SerializeField]
	private float spinSpeed = 1000f;

	private AudioSource audio;

	[SerializeField]
	private Transform torndoObject;

	private float yPos = 5.85f;
	private float minZ = 0.66f;
	private float maxZ = -10f;
	private float minX = 18.13f;
	private float maxX = 477.13f;

	[SerializeField]
	private float turnSpeed = 0.4f;
	[SerializeField]
	private float moveSpeed = 1f;

	public FollowLeader leader;

	void Start () {
		audio = GetComponent<AudioSource>();

		if (leader == null) {
			leader = FindObjectOfType<FollowLeader>();
		}
	}

	void Update () {
		torndoObject.Rotate(0, 0, spinSpeed * Time.deltaTime);

		if (leader.leader != null && leader.leader.GetComponent<Biker>() != null) {
			transform.LookAt(leader.leader);
		} else {
			return;
		}

		transform.position += transform.forward * Time.deltaTime * moveSpeed;
	}

	void OnTriggerEnter (Collider col) {
		if (col.GetComponent<Biker>() != null) {
			Hit (col.GetComponent<Biker>());
		}
	}

	private void Hit (Biker biker) {
		// Slow the biker down
		biker.Pedal(-4);
		audio.Play();
	}
}
