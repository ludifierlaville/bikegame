﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowLeader : MonoBehaviour {

    //use speed factor to move lines faster than than actual biker to give speed illusion
    public float speedFactor = 1f;
    public float offset = 0;

    private float leadPosition = 0f;
	public Transform leader;

	void Start () {
		
	}
	
	void Update () 
    {		
        //find who's lead and its current position
		for(int i=0; i < GameController.instance.bikers.Length; i++)
		{
			if (GameController.instance.bikers[i].transform.position.x > leadPosition)
				leader = GameController.instance.bikers[i].transform;

            leadPosition = Mathf.Max(leadPosition, GameController.instance.bikers[i].transform.position.x);
	    }

        //move to leader's position
		Vector3 pos = transform.position;
        pos.x = leadPosition * speedFactor + offset;
		transform.position = pos;
    }

    public float GetLeadPosition()
    {
        return leadPosition;
    }
}
