﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Biker : MonoBehaviour {
	private float pedalforce = 1f;

	private Rigidbody rb;

	public GameObject guidon;
	public GameObject frontWheel;
	public GameObject backWheel;
	public GameObject rightGuidonTarget;
	public GameObject leftGuidonTarget;
	public GameObject defaultGuidonTarget;

	[SerializeField]
	private SkinnedMeshRenderer playerSMR;

	public enum PlayerColor {
		Red,
		Blue,
		Green,
		Yellow
	}

	public PlayerColor playerColor;

	void Start () {
		rb = GetComponent<Rigidbody>();

		if (this.tag != "Biker") {
			Debug.LogError("Set the tag to \"Biker\"");
		}

		pedalforce = GameController.defaultpedalForce;


		// Assign color
		if (playerColor == PlayerColor.Red)
			playerSMR.material.color = Color.red;
		else if (playerColor == PlayerColor.Blue)
			playerSMR.material.color = Color.blue;
		else if (playerColor == PlayerColor.Green)
			playerSMR.material.color = Color.green;
		else
			playerSMR.material.color = Color.yellow;
	}

	public void Pedal () {
		Pedal(1f);
	}

	public void Pedal (float mult) {
		rb.AddForce(pedalforce * mult, 0, 0);
	}

	void OnCollisionEnter (Collision col) {
		Debug.Log("***************** " + col.collider.name + " is the thing messing everything up");
	}
}