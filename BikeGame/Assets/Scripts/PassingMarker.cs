﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassingMarker : MonoBehaviour {

    public MeteoDataController meteo;
    public FollowLeader leader;
    bool triggered;

	// Use this for initialization
	void Start () 
    {
        triggered = false;
        if (meteo == null)
        {
            meteo = FindObjectOfType<MeteoDataController>();
        }
        if (leader == null)
        {
            leader = FindObjectOfType<FollowLeader>();
        }
	}
	
	// Update is called once per frame
    void Update()
    {
        if (triggered)
        {
            return;
        }

        //change the day when leader pass me
        float leadPosition = leader.GetLeadPosition();
        if(leadPosition > transform.position.x)
        {
            triggered = true;

            //TODO: increment day
            meteo.IncrementDay();

            //DEBUG, change color
            GetComponent<MeshRenderer>().material.color = new Color(1f, 0f, 0f);
        }
    }
}
