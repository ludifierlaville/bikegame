﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	public enum PowerUpType {
		WaterBottle,
		EnergyDrink,
		COUNT
	};
	public PowerUpType powerUpType;

	List<Transform> children = new List<Transform>();

	public Texture[] starTextures;
	public MeshRenderer mr;
	public AudioSource audio;

	void Start () {
		// Set all the children to true so that we can add them to list
		foreach (Transform child in transform) {
			child.gameObject.SetActive(true);
		}

		foreach (Transform child in transform) {
			children.Add(child);
		}

		// Set all the children to false
		foreach (Transform child in transform) {
			child.gameObject.SetActive(false);
		}

		DisplayPowerUp (powerUpType.ToString());
	}

	public void DisplayPowerUp (string powerUpName) {
		foreach (Transform child in transform) {
			if (child.gameObject.name == powerUpName) {
				child.gameObject.SetActive(true);
				mr = child.GetComponent<MeshRenderer>();
				audio = child.GetComponent<AudioSource>();
				children.Clear();
				return;
			}
		}
		Debug.Log("No children named " + powerUpName);
	}

	void OnTriggerEnter (Collider col) {
		if (col.GetComponent<Biker>() != null) {
			PickUp (col.GetComponent<Biker>(), powerUpType.ToString());
		}
	}

	public void PickUp (Biker biker, string powerUp) {
		audio.Play();

		if (powerUp == "WaterBottle")
			biker.Pedal(2);
		else // Energy drank
			biker.Pedal(3);

		StartCoroutine(ShowStar());
	}

	private IEnumerator ShowStar () {
		for (int i = 0; i < starTextures.Length; i++) {
			mr.material.SetTexture("_MainTex", starTextures[i]);
			yield return new WaitForSeconds(0.1f);
		}
		Destroy(this.gameObject);
		yield break;
	}
}
