﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragStraggler : MonoBehaviour {

    //the leader will drag the straggler with him,
    //thus preventing that straggler from leaving the camera's view
    public FollowLeader leader;

    float maxDifference = 10f;

	// Use this for initialization
	void Start ()
    {
        if (leader == null)
        {
            leader = Camera.main.GetComponent<FollowLeader>();
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        //is too far behind
        Vector3 pos = transform.position;
        if (pos.x < leader.GetLeadPosition() - maxDifference)
        {
            //push him to the edge
            pos.x = leader.GetLeadPosition() - maxDifference;
            transform.position = pos;
        }
	}
}
