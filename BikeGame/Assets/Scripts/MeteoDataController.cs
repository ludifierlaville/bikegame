﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;  

public class MeteoDataController : MonoBehaviour
{
    public TextMesh currentDay;
    public TextMesh skyCondition;
    public TextMesh temperature;
    public SpriteRenderer skyIcon;
    //public TextMesh windSpeed;

    public EmitSkyParticles pSystem;
    public SunMoonController sunMoonRot;
    
    MeteoData[] weeklyMeteo;
    int currentDayIndex = 0;
    int daysCount = 0;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        weeklyMeteo = new MeteoData[14];

		Load(Application.dataPath + "/Data/meteo_db.txt");
        DisplayMeteoData();
    }
    
    bool Load(string fileName)
    {
        daysCount = 0;

        // Handle any problems that might arise when reading the text
        try
        {
            string line;
            // Create a new StreamReader, tell it which file to read and what encoding the file was saved as
            StreamReader theReader = new StreamReader(fileName, Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            using (theReader)
            {
                //skip first line, it's comments
                theReader.ReadLine();

                // While there's lines left in the text file, do this:
                do
                {
                    //read the day
                    line = theReader.ReadLine();
                    if (line != null)
                    {
                        //create a new data item first
                        weeklyMeteo[daysCount] = new MeteoData();

                        // Do whatever you need to do with the text line, it's a string now
                        string entry = line.Substring(0, line.IndexOf(':'));
                        if (entry.Length > 0)
                        {
                            weeklyMeteo[daysCount].day = entry;
                        }
                    }
                    
                    //read the meteo for that day
                    line = theReader.ReadLine();
                    if (line != null)
                    {
                        // Do whatever you need to do with the text line, it's a string now
                        string[] entries = line.Split(',');
                        if (entries.Length > 0)
                        {
                            LoadInfos(entries);
                        }
                    }

                    ++daysCount;
                }
                while (line != null);

                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return true;
            }
        }
        catch (System.Exception e)
        {
            System.Console.WriteLine("{0}\n", e.Message);
            return false;
        }
    }

    void LoadInfos(string[] entries)
    {
        entries[0] = Regex.Replace(entries[0], @"\s+", "");
        weeklyMeteo[daysCount].skyCondition = entries[0];
        entries[1] = Regex.Replace(entries[1], @"\s+", "");
        weeklyMeteo[daysCount].temperature = int.Parse(entries[1]);
        entries[2] = Regex.Replace(entries[2], @"\s+", "");
        weeklyMeteo[daysCount].wind = int.Parse(entries[2]);
    }

    public void DisplayMeteoData()
    {
        currentDay.text = weeklyMeteo[currentDayIndex].day;

        skyCondition.text = weeklyMeteo[currentDayIndex].skyCondition;
        temperature.text = weeklyMeteo[currentDayIndex].temperature + "°C";
        //windSpeed.text = weeklyMeteo[currentDayIndex].wind + "km/h";

        //fetch the sky icon with the matching name
        skyIcon.sprite = Resources.Load(skyCondition.text, typeof(Sprite)) as Sprite;

        //also reflect meteo in the game
        pSystem.OnNewDay(skyCondition.text);
    }

    public void IncrementDay()
    {
        ++currentDayIndex;
        //safety net, loop back to today's weather
        if (weeklyMeteo[currentDayIndex] == null)
        {
            currentDayIndex = 0;
        }
        DisplayMeteoData();

        //also reset the sun and moon anim
        sunMoonRot.reset();
    }
}