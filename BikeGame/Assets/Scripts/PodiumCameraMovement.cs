﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodiumCameraMovement : MonoBehaviour {

	[SerializeField]
	private float rotateSpeed = 15f;

	void Update () {
		transform.Rotate(0, Mathf.Sin(Time.time) * Time.deltaTime * rotateSpeed, 0);
	}
}
