﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MeteoData
{
    public string day;
    public string skyCondition;
    public int temperature;
    public int wind;
}